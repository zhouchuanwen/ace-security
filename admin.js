// 1、http://localhost:9527/api/auth/jwt/token
// 在AuthController 类里 @RequestMapping("jwt")
/*
1)在/ace-security/ace-auth/ace-auth-server/src/auth/controller/AuthController.java里
类开头:@RequestMapping("jwt"), 方法:

@Autowired
private AuthService authService;

@RequestMapping(value = "token", method = RequestMethod.POST)
public ObjectRestResponse<String> createAuthenticationToken(
        @RequestBody JwtAuthenticationRequest authenticationRequest) throws Exception {
    log.info(authenticationRequest.getUsername()+" require logging...");
    final String token = authService.login(authenticationRequest);
    return new ObjectRestResponse<>().data(token);
}
2) /ace-security/ace-auth/ace-auth-server/auth/util/user/JwtAuthenticationRequest.java里定义了请求body的参数及类型JwtAuthenticationRequest
   可以通过authenticationRequest.getUsername() 获取用户名，调用 authService.login(authenticationRequest); 获取token
3）/ace-security/ace-auth/ace-auth-server/auth/service/AuthService 接口文件 有login方法，但没有实现
    其实现在 impl/AuthServiceImpl.javal里，如下
    private JwtTokenUtil jwtTokenUtil;
    private IUserService userService;
    @Override
    public String login(JwtAuthenticationRequest authenticationRequest) throws Exception {
        UserInfo info = userService.validate(authenticationRequest);
        if (!StringUtils.isEmpty(info.getId())) {
            return jwtTokenUtil.generateToken(new JWTInfo(info.getUsername(), info.getId() + "", info.getName()));
        }
        throw new UserInvalidException("用户不存在或账户密码错误!");
    }
4）调用 ace-auth-server/feign/IUserService接口里的validate()方法。具体待解释，返回UserInfo用户信息，在
    /ace-security/ace-modules/ace-interface/src/security/api/vo/user/UserInfo.java类里
5）如果返回的userid不为空，调用/ace-security/ace-auth/ace-auth-server/auth/util/user/JwtTokenUtil路径下的generateToken()方法， 
jwtTokenUtil.generateToken(new JWTInfo(info.getUsername(), info.getId() + "", info.getName()));
6）jwtTokenUtil下的  generateToken方法()
    public String generateToken(IJWTInfo jwtInfo) throws Exception {
        return JWTHelper.generateToken(jwtInfo, keyConfiguration.getUserPriKey(),expire);
    }
    JWTHelper在/ace-security/ace-auth/ace-auth-common/auth/common/util/jwt/JWTHelper.java下
    generateToken()接收三个参数，一个jwtInfo，一个私钥，一个过期时间
    @Value("${jwt.expire}")  // 单位秒14400为4小时， .setExpiration(DateTime.now().plusSeconds(expire).toDate()) 
    private int expire;  过期时间
    @Autowired
    private KeyConfiguration keyConfiguration; 
    该路径/ace-security/ace-auth/ace-auth-server/src/main/java/com/github/wxiaoqi/security/auth/configuration/KeyConfiguration.java下：
    过期时间和RSA私钥 存在了/ace-security/ace-auth/ace-auth-server/src/main/resources/application.yml里
7）逐级返回到 AuthController.java，拿到token后，调用/ace-security/ace-common/security/common/msg/ObjectRestResponse.java
    里的data组装返回数据
*/
Post接口
请求
let request= {username: "admin", password: "admin"}
let response={
    "status":200,
    "data":"eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInVzZXJJZCI6IjEiLCJuYW1lIjoiTXIuQUciLCJleHAiOjE1Mjc5NDIyMTZ9.leT61BP4uczK7DtkW3L7BiMELc2j0q_kUQYWNh7KDTgyKI_JPpmdo5KcGqZoAtFEpnN0mQE2gul6hJFJ3h-FH3XoOC28QepPKNk2vvyp1ODBqNI5BAJhPg6cYJm6sOTDOpb0KDPKFOifJwk8FaHbAaZKBD5KSQD1JHqD4v_w6Gw",
    "rel":false
}

// 2、Get接口
// http://localhost:9527/api/admin/user/front/info?token=eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInVzZXJJZCI6IjEiLCJuYW1lIjoiTXIuQUciLCJleHAiOjE1Mjc5NDIyMTZ9.leT61BP4uczK7DtkW3L7BiMELc2j0q_kUQYWNh7KDTgyKI_JPpmdo5KcGqZoAtFEpnN0mQE2gul6hJFJ3h-FH3XoOC28QepPKNk2vvyp1ODBqNI5BAJhPg6cYJm6sOTDOpb0KDPKFOifJwk8FaHbAaZKBD5KSQD1JHqD4v_w6Gw
// 在UserController类里 @RequestMapping("user")
/*
    在/ace-security/ace-modules/ace-admin/security/admin/rest/UserController.java 里
    @RequestMapping("user")
    方法实现如下
     @Autowired
    private PermissionService permissionService;

    @RequestMapping(value = "/front/info", method = RequestMethod.GET) // 对于路径和方法类型
    @ResponseBody
    public ResponseEntity<?> getUserInfo(String token) throws Exception {
        FrontUser userInfo = permissionService.getUserInfo(token);
        if(userInfo==null) {
            return ResponseEntity.status(401).body(false);
        } else {
            return ResponseEntity.ok(userInfo);
        }
    }
    1）调用PermissionService类里的getUserInfo，根据token获取用户信息（路径：/ace-security/ace-modules/ace-admin/security/admin/rpc/service/PermissionService.java）
    2）然后用第三方库了里的ResponseEntity.ok(userInfo)返回数据（路径：/Users/zcwmac/.m2/repository/org/springframework/spring-web/5.0.4.RELEASE/spring-web-5.0.4.RELEASE-sources.jar!/org/springframework/http/ResponseEntity.java）
    3）其中PermissionService类里的getUserInfo实现如下：
    public FrontUser getUserInfo(String token) throws Exception {
        4) 在/Users/zcwmac/ace-security/ace-auth/ace-auth-client/src/main/java/com/github/wxiaoqi/security/auth/client/jwt/UserAuthUtil.java
        里将token解析出信息，得到username
        String username = userAuthUtil.getInfoFromToken(token).getUniqueName();
        if (username == null) {
            return null;
        }
        UserInfo user = this.getUserByUsername(username);
        FrontUser frontUser = new FrontUser();
        5）用第三方BeanUtils的copyProperties快速拷贝 /Users/zcwmac/.m2/repository/org/springframework/spring-beans/5.0.4.RELEASE/spring-beans-5.0.4.RELEASE-sources.jar!/org/springframework/beans/BeanUtils.java
        BeanUtils.copyProperties(user, frontUser);
        List<PermissionInfo> permissionInfos = this.getPermissionByUsername(username);
        Stream<PermissionInfo> menus = permissionInfos.parallelStream().filter((permission) -> {
            return permission.getType().equals(CommonConstants.RESOURCE_TYPE_MENU);
        });
        frontUser.setMenus(menus.collect(Collectors.toList()));
        Stream<PermissionInfo> elements = permissionInfos.parallelStream().filter((permission) -> {
            return !permission.getType().equals(CommonConstants.RESOURCE_TYPE_MENU);
        });
        frontUser.setElements(elements.collect(Collectors.toList()));
        return frontUser;
    }
*/
response={
    "id":"1",
    "username":"admin",
    "name":"Mr.AG",
    "description":"",
    "menus":[
        {
            "code":"userManager",
            "type":"menu",
            "uri":"/admin/user",
            "method":"GET",
            "name":"访问",
            "menu":"用户管理"
        },
        {
            "code":"baseManager",
            "type":"menu",
            "uri":"/admin",
            "method":"GET",
            "name":"访问",
            "menu":"基础配置管理"
        },
        {
            "code":"menuManager",
            "type":"menu",
            "uri":"/admin/menu",
            "method":"GET",
            "name":"访问",
            "menu":"菜单管理"
        },
        {
            "code":"groupManager",
            "type":"menu",
            "uri":"/admin/group",
            "method":"GET",
            "name":"访问",
            "menu":"角色权限管理"
        },
        {
            "code":"groupTypeManager",
            "type":"menu",
            "uri":"/admin/groupType",
            "method":"GET",
            "name":"访问",
            "menu":"角色类型管理"
        },
        {
            "code":"adminSys",
            "type":"menu",
            "uri":"/base",
            "method":"GET",
            "name":"访问",
            "menu":"权限管理系统"
        },
        {
            "code":"gateLogManager",
            "type":"menu",
            "uri":"/admin/gateLog",
            "method":"GET",
            "name":"访问",
            "menu":"操作日志"
        },
        {
            "code":"authManager",
            "type":"menu",
            "uri":"/auth",
            "method":"GET",
            "name":"访问",
            "menu":"服务权限管理"
        },
        {
            "code":"serviceManager",
            "type":"menu",
            "uri":"/auth/service",
            "method":"GET",
            "name":"访问",
            "menu":"服务管理"
        },
        {
            "code":"monitorManager",
            "type":"menu",
            "uri":"/monitorManager",
            "method":"GET",
            "name":"访问",
            "menu":"监控模块管理"
        },
        {
            "code":"serviceEurekaManager",
            "type":"menu",
            "uri":"/serviceEurekaManager",
            "method":"GET",
            "name":"访问",
            "menu":"服务注册中心"
        },
        {
            "code":"serviceMonitorManager",
            "type":"menu",
            "uri":"/serviceMonitorManager",
            "method":"GET",
            "name":"访问",
            "menu":"服务状态监控"
        },
        {
            "code":"serviceZipkinManager",
            "type":"menu",
            "uri":"/serviceZipkinManager",
            "method":"GET",
            "name":"访问",
            "menu":"服务链路监控"
        }
    ],
    "elements":[
        {
            "code":"userManager:btn_del",
            "type":"button",
            "uri":"/admin/user/{*}",
            "method":"DELETE",
            "name":"删除",
            "menu":"用户管理"
        },
        {
            "code":"menuManager:element",
            "type":"uri",
            "uri":"/admin/element",
            "method":"GET",
            "name":"按钮页面",
            "menu":"菜单管理"
        },
        {
            "code":"menuManager:btn_add",
            "type":"button",
            "uri":"/admin/menu/{*}",
            "method":"POST",
            "name":"新增",
            "menu":"菜单管理"
        },
        {
            "code":"menuManager:btn_edit",
            "type":"button",
            "uri":"/admin/menu/{*}",
            "method":"PUT",
            "name":"编辑",
            "menu":"菜单管理"
        },
        {
            "code":"menuManager:btn_del",
            "type":"button",
            "uri":"/admin/menu/{*}",
            "method":"DELETE",
            "name":"删除",
            "menu":"菜单管理"
        },
        {
            "code":"menuManager:btn_element_add",
            "type":"button",
            "uri":"/admin/element",
            "method":"POST",
            "name":"新增元素",
            "menu":"菜单管理"
        },
        {
            "code":"menuManager:btn_element_edit",
            "type":"button",
            "uri":"/admin/element/{*}",
            "method":"PUT",
            "name":"编辑元素",
            "menu":"菜单管理"
        },
        {
            "code":"menuManager:btn_element_del",
            "type":"button",
            "uri":"/admin/element/{*}",
            "method":"DELETE",
            "name":"删除元素",
            "menu":"菜单管理"
        },
        {
            "code":"groupManager:btn_add",
            "type":"button",
            "uri":"/admin/group",
            "method":"POST",
            "name":"新增",
            "menu":"角色权限管理"
        },
        {
            "code":"groupManager:btn_edit",
            "type":"button",
            "uri":"/admin/group/{*}",
            "method":"PUT",
            "name":"编辑",
            "menu":"角色权限管理"
        },
        {
            "code":"groupManager:menu",
            "type":"uri",
            "uri":"/admin/group/{*}/authority/menu",
            "method":"POST",
            "name":"分配菜单",
            "menu":"角色权限管理"
        },
        {
            "code":"groupManager:element",
            "type":"uri",
            "uri":"/admin/group/{*}/authority/element",
            "method":"POST",
            "name":"分配资源",
            "menu":"角色权限管理"
        },
        {
            "code":"userManager:view",
            "type":"uri",
            "uri":"/admin/user/{*}",
            "method":"GET",
            "name":"查看",
            "menu":"用户管理"
        },
        {
            "code":"menuManager:view",
            "type":"uri",
            "uri":"/admin/menu/{*}",
            "method":"GET",
            "name":"查看",
            "menu":"菜单管理"
        },
        {
            "code":"menuManager:element_view",
            "type":"uri",
            "uri":"/admin/element/{*}",
            "method":"GET",
            "name":"查看",
            "menu":"菜单管理"
        },
        {
            "code":"groupManager:view",
            "type":"uri",
            "uri":"/admin/group/{*}",
            "method":"GET",
            "name":"查看",
            "menu":"角色权限管理"
        },
        {
            "code":"groupTypeManager:view",
            "type":"uri",
            "uri":"/admin/groupType/{*}",
            "method":"GET",
            "name":"查看",
            "menu":"角色类型管理"
        },
        {
            "code":"groupTypeManager:btn_add",
            "type":"button",
            "uri":"/admin/groupType",
            "method":"POST",
            "name":"新增",
            "menu":"角色类型管理"
        },
        {
            "code":"groupTypeManager:btn_edit",
            "type":"button",
            "uri":"/admin/groupType/{*}",
            "method":"PUT",
            "name":"编辑",
            "menu":"角色类型管理"
        },
        {
            "code":"groupTypeManager:btn_del",
            "type":"button",
            "uri":"/admin/groupType/{*}",
            "method":"DELETE",
            "name":"删除",
            "menu":"角色类型管理"
        },
        {
            "code":"gateLogManager:view",
            "type":"button",
            "uri":"/admin/gateLog",
            "method":"GET",
            "name":"查看",
            "menu":"操作日志"
        },
        {
            "code":"serviceManager:btn_add",
            "type":"button",
            "uri":"/auth/service",
            "method":"POST",
            "name":"新增",
            "menu":"服务管理"
        },
        {
            "code":"serviceManager:btn_edit",
            "type":"button",
            "uri":"/auth/service/{*}",
            "method":"PUT",
            "name":"编辑",
            "menu":"服务管理"
        },
        {
            "code":"userManager:btn_add",
            "type":"button",
            "uri":"/admin/user",
            "method":"POST",
            "name":"新增",
            "menu":"用户管理"
        },
        {
            "code":"userManager:btn_edit",
            "type":"button",
            "uri":"/admin/user/{*}",
            "method":"PUT",
            "name":"编辑",
            "menu":"用户管理"
        },
        {
            "code":"serviceManager:view",
            "type":"URI",
            "uri":"/auth/service/{*}",
            "method":"GET",
            "name":"查看",
            "menu":"服务管理"
        },
        {
            "code":"serviceManager:btn_del",
            "type":"button",
            "uri":"/auth/service/{*}",
            "method":"DELETE",
            "name":"删除",
            "menu":"服务管理"
        },
        {
            "code":"serviceManager:btn_clientManager",
            "type":"button",
            "uri":"/auth/service/{*}/client",
            "method":"POST",
            "name":"服务授权",
            "menu":"服务管理"
        }
    ]
}

// 3、GET接口
// http://localhost:9527/api/admin/user/front/menus?token=eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInVzZXJJZCI6IjEiLCJuYW1lIjoiTXIuQUciLCJleHAiOjE1Mjc5NDIyMTZ9.leT61BP4uczK7DtkW3L7BiMELc2j0q_kUQYWNh7KDTgyKI_JPpmdo5KcGqZoAtFEpnN0mQE2gul6hJFJ3h-FH3XoOC28QepPKNk2vvyp1ODBqNI5BAJhPg6cYJm6sOTDOpb0KDPKFOifJwk8FaHbAaZKBD5KSQD1JHqD4v_w6Gw
// 在UserController类里 @RequestMapping("user")
/***
    @RequestMapping(value = "/front/menus", method = RequestMethod.GET)
    public @ResponseBody
    List<MenuTree> getMenusByUsername(String token) throws Exception {
        return permissionService.getMenusByUsername(token);
    }
    public List<MenuTree> getMenusByUsername(String token) throws Exception {
        String username = userAuthUtil.getInfoFromToken(token).getUniqueName();
        if (username == null) {
            return null;
        }
        User user = userBiz.getUserByUsername(username);
        List<Menu> menus = menuBiz.getUserAuthorityMenuByUserId(user.getId());
        return getMenuTree(menus,AdminCommonConstant.ROOT);
    }
 */
response = [
    {
        "id":13,
        "parentId":-1,
        "children":[
            {
                "id":5,
                "parentId":13,
                "children":[
                    {
                        "id":1,
                        "parentId":5,
                        "children":[

                        ],
                        "icon":"fa-user",
                        "title":"用户管理",
                        "href":"/admin/user",
                        "spread":false,
                        "path":"/adminSys/baseManager/userManager",
                        "code":"userManager"
                    },
                    {
                        "id":6,
                        "parentId":5,
                        "children":[

                        ],
                        "icon":"category",
                        "title":"菜单管理",
                        "href":"/admin/menu",
                        "spread":false,
                        "path":"/adminSys/baseManager/menuManager",
                        "code":"menuManager"
                    },
                    {
                        "id":7,
                        "parentId":5,
                        "children":[

                        ],
                        "icon":"group_fill",
                        "title":"角色权限管理",
                        "href":"/admin/group",
                        "spread":false,
                        "path":"/adminSys/baseManager/groupManager",
                        "code":"groupManager"
                    },
                    {
                        "id":8,
                        "parentId":5,
                        "children":[

                        ],
                        "icon":"fa-users",
                        "title":"角色类型管理",
                        "href":"/admin/groupType",
                        "spread":false,
                        "path":"/adminSys/baseManager/groupTypeManager",
                        "code":"groupTypeManager"
                    },
                    {
                        "id":27,
                        "parentId":5,
                        "children":[

                        ],
                        "icon":"viewlist",
                        "title":"操作日志",
                        "href":"/admin/gateLog",
                        "spread":false,
                        "path":"/adminSys/baseManager/gateLogManager",
                        "code":"gateLogManager"
                    }
                ],
                "icon":"setting",
                "title":"基础配置管理",
                "href":"/admin",
                "spread":false,
                "path":"/adminSys/baseManager",
                "code":"baseManager"
            },
            {
                "id":29,
                "parentId":13,
                "children":[
                    {
                        "id":30,
                        "parentId":29,
                        "children":[

                        ],
                        "icon":"client",
                        "title":"服务管理",
                        "href":"/auth/service",
                        "spread":false,
                        "path":"/adminSys/authManager/serviceManager",
                        "code":"serviceManager"
                    }
                ],
                "icon":"service",
                "title":"服务权限管理",
                "href":"/auth",
                "spread":false,
                "path":"/adminSys/authManager",
                "code":"authManager"
            },
            {
                "id":31,
                "parentId":13,
                "children":[
                    {
                        "id":32,
                        "parentId":31,
                        "children":[

                        ],
                        "icon":"client",
                        "title":"服务注册中心",
                        "spread":false,
                        "path":"/adminSys/monitorManager/serviceEurekaManager",
                        "code":"serviceEurekaManager"
                    },
                    {
                        "id":33,
                        "parentId":31,
                        "children":[

                        ],
                        "icon":"client",
                        "title":"服务状态监控",
                        "spread":false,
                        "path":"/adminSys/monitorManager/serviceEurekaManager",
                        "code":"serviceMonitorManager"
                    },
                    {
                        "id":34,
                        "parentId":31,
                        "children":[

                        ],
                        "icon":"client",
                        "title":"服务链路监控",
                        "spread":false,
                        "path":"/adminSys/monitorManager/serviceZipkinManager",
                        "code":"serviceZipkinManager"
                    }
                ],
                "icon":"service",
                "title":"监控模块管理",
                "spread":false,
                "path":"/adminSys/monitorManager",
                "code":"monitorManager"
            }
        ],
        "icon":"setting",
        "title":"权限管理系统",
        "href":"/base",
        "spread":false,
        "path":"/adminSys",
        "code":"adminSys"
    }
]

// 4、GET接口 
// http://localhost:9527/api/admin/user/front/menu/all
// 在UserController类里 @RequestMapping("user")
/**
    @RequestMapping(value = "/front/menu/all", method = RequestMethod.GET)
    public @ResponseBody
    List<Menu> getAllMenus() throws Exception {
        return menuBiz.selectListAll();
    }
    menuBiz路径/Users/zcwmac/ace-security/ace-modules/ace-admin/src/main/java/com/github/wxiaoqi/security/admin/biz/MenuBiz.java
 */
response = [
    {
        "id":1,
        "code":"userManager",
        "title":"用户管理",
        "parentId":5,
        "href":"/admin/user",
        "icon":"fa-user",
        "type":"menu",
        "description":"",
        "updTime":"2017-09-05 21:06:51",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "attr1":"_import('admin/user/index')",
        "path":"/adminSys/baseManager/userManager"
    },
    {
        "id":5,
        "code":"baseManager",
        "title":"基础配置管理",
        "parentId":13,
        "href":"/admin",
        "icon":"setting",
        "type":"dirt",
        "description":"",
        "updTime":"2017-09-05 21:46:11",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "attr1":"Layout",
        "path":"/adminSys/baseManager"
    },
    {
        "id":6,
        "code":"menuManager",
        "title":"菜单管理",
        "parentId":5,
        "href":"/admin/menu",
        "icon":"category",
        "type":"menu",
        "description":"",
        "updTime":"2017-09-05 21:10:25",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "attr1":"_import('admin/menu/index')",
        "path":"/adminSys/baseManager/menuManager"
    },
    {
        "id":7,
        "code":"groupManager",
        "title":"角色权限管理",
        "parentId":5,
        "href":"/admin/group",
        "icon":"group_fill",
        "type":"menu",
        "description":"",
        "updTime":"2017-09-05 21:11:34",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "attr1":"import('admin/group/index')",
        "path":"/adminSys/baseManager/groupManager"
    },
    {
        "id":8,
        "code":"groupTypeManager",
        "title":"角色类型管理",
        "parentId":5,
        "href":"/admin/groupType",
        "icon":"fa-users",
        "type":"menu",
        "description":"",
        "updTime":"2017-09-05 21:12:28",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "attr1":"_import('admin/groupType/index')",
        "path":"/adminSys/baseManager/groupTypeManager"
    },
    {
        "id":13,
        "code":"adminSys",
        "title":"权限管理系统",
        "parentId":-1,
        "href":"/base",
        "icon":"setting",
        "type":"dirt",
        "description":"",
        "updTime":"2017-09-28 12:09:22",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "attr1":"Layout",
        "path":"/adminSys"
    },
    {
        "id":21,
        "code":"dictManager",
        "title":"数据字典",
        "parentId":5,
        "href":"",
        "icon":"fa fa-book",
        "description":"",
        "path":"/adminSys/baseManager/dictManager"
    },
    {
        "id":27,
        "code":"gateLogManager",
        "title":"操作日志",
        "parentId":5,
        "href":"/admin/gateLog",
        "icon":"viewlist",
        "type":"menu",
        "description":"",
        "crtTime":"2017-07-01 00:00:00",
        "crtUser":"1",
        "crtName":"admin",
        "crtHost":"0:0:0:0:0:0:0:1",
        "updTime":"2017-09-05 22:32:55",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "attr1":"_import('admin/gateLog/index')",
        "path":"/adminSys/baseManager/gateLogManager"
    },
    {
        "id":29,
        "code":"authManager",
        "title":"服务权限管理",
        "parentId":13,
        "href":"/auth",
        "icon":"service",
        "description":"服务权限管理",
        "crtTime":"2017-12-26 19:54:45",
        "crtUser":"1",
        "crtName":"Mr.AG",
        "crtHost":"127.0.0.1",
        "updTime":"2017-12-26 19:55:18",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "path":"/adminSys/authManager"
    },
    {
        "id":30,
        "code":"serviceManager",
        "title":"服务管理",
        "parentId":29,
        "href":"/auth/service",
        "icon":"client",
        "description":"服务管理",
        "crtTime":"2017-12-26 19:56:06",
        "crtUser":"1",
        "crtName":"Mr.AG",
        "crtHost":"127.0.0.1",
        "updTime":"2017-12-26 19:56:06",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "path":"/adminSys/authManager/serviceManager"
    },
    {
        "id":31,
        "code":"monitorManager",
        "title":"监控模块管理",
        "parentId":13,
        "icon":"service",
        "crtTime":"2018-02-25 09:36:35",
        "crtUser":"1",
        "crtName":"Mr.AG",
        "crtHost":"127.0.0.1",
        "updTime":"2018-02-25 09:38:55",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "path":"/adminSys/monitorManager"
    },
    {
        "id":32,
        "code":"serviceEurekaManager",
        "title":"服务注册中心",
        "parentId":31,
        "icon":"client",
        "crtTime":"2018-02-25 09:37:04",
        "crtUser":"1",
        "crtName":"Mr.AG",
        "crtHost":"127.0.0.1",
        "updTime":"2018-02-25 09:37:41",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "path":"/adminSys/monitorManager/serviceEurekaManager"
    },
    {
        "id":33,
        "code":"serviceMonitorManager",
        "title":"服务状态监控",
        "parentId":31,
        "icon":"client",
        "crtTime":"2018-02-25 09:37:05",
        "crtUser":"1",
        "crtName":"Mr.AG",
        "crtHost":"127.0.0.1",
        "updTime":"2018-02-25 09:37:35",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "path":"/adminSys/monitorManager/serviceEurekaManager"
    },
    {
        "id":34,
        "code":"serviceZipkinManager",
        "title":"服务链路监控",
        "parentId":31,
        "icon":"client",
        "crtTime":"2018-02-25 09:38:05",
        "crtUser":"1",
        "crtName":"Mr.AG",
        "crtHost":"127.0.0.1",
        "updTime":"2018-02-25 09:38:05",
        "updUser":"1",
        "updName":"Mr.AG",
        "updHost":"127.0.0.1",
        "path":"/adminSys/monitorManager/serviceZipkinManager"
    }
]